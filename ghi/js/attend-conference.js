window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }

      const completeLoading = document.getElementById('loading-conference-spinner')
      // Here, add the 'd-none' class to the loading icon
      completeLoading.classList.add('d-none')
      // Here, remove the 'd-none' class from the select tag
      selectTag.classList.remove('d-none')

      //Get the attendee form element by its id
      const formTag = document.getElementById('create-attendee-form');
      //Add an event handler for the submit event
      formTag.addEventListener('submit', async event => {
        //Prevent the default from happening
          event.preventDefault();
          //Create a FormData object from the form
          const formData = new FormData (formTag);
          //Get a new object from the form data's entries
          const json = JSON.stringify(Object.fromEntries(formData));
          const attendeeUrl = 'http://localhost:8001/api/attendees/';
          //Create options for the fetch
          const fetchConfig = {
              method: "post",
              body: json,
              headers: {
                  'Content-Type': 'application/json',
              },
          };
          //Make the fetch using the await keyword to the URL
          const attendeeResponse = await fetch(attendeeUrl, fetchConfig);
          const alertSuccess = document.getElementById('success-message')
          //When the response from the fetch is good
          if (attendeeResponse.ok) {
              formTag.reset();
              formTag.classList.add('d-none')
              alertSuccess.classList.remove('d-none')
              const newAttendee = await attendeeResponse.json();
              console.log(newAttendee);
          }
  });
}

});
