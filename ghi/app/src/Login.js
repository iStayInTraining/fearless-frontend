import React, { useState } from "react";

function Login() {
  const [formData, setFormData] = useState({
    username: "",
    password: "",
  });

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevFormData) => ({
      ...prevFormData,
      [name]: value,
    }));
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const fetchConfig = {
      credentials: "include",
      method: "post",
      body: JSON.stringify(formData), // Convert the data to JSON
      headers: {
        "Content-Type": "application/json",
      },
    };
    const url = "http://localhost:8000/login/";
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      window.location.href = "/"; // Redirect to home page on successful login
    } else {
      console.error(response);
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Login</h1>
          <form onSubmit={handleSubmit}>
            <div className="form-floating mb-3">
              <input
                placeholder="Username"
                required
                type="text"
                name="username"
                id="username"
                className="form-control"
                value={formData.username}
                onChange={handleInputChange}
              />
              <label htmlFor="username">Username</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Password"
                required
                type="password"
                name="password"
                id="password"
                className="form-control"
                value={formData.password}
                onChange={handleInputChange}
              />
              <label htmlFor="password">Password</label>
            </div>
            <button className="btn btn-primary">Login</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default Login;
