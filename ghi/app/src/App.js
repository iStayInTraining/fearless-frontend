import Nav from './Nav';
import React from 'react';
import './App.css';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendeeForm from './AttendeeForm';
import PresentationForm from './PresentationForm';
import Login from './Login';
import MainPage from './MainPage';import {
   BrowserRouter,
   Routes,
   Route,
  } from 'react-router-dom';


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/login" element={<Login />} />
          <Route path="/attendees" element={<AttendeesList attendees={props.attendees} />} />
          <Route path="/attendees/new" element={<AttendeeForm />} />
          <Route path="/locations/new" element={<LocationForm />} />
          <Route path="/conferences/new" element={<ConferenceForm />} />
          <Route path="/presentations/new" element={<PresentationForm />} />
        </Routes>
    </BrowserRouter>
  );
}

export default App;
