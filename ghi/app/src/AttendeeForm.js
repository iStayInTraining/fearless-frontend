import React, { useEffect, useState } from "react";

function AttendeeForm() {
  const [conferences, setConferences] = useState([]);
  const [loading, setLoading] = useState(true);
  const [selectedConference, setSelectedConference] = useState("");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [success, setSuccess] = useState(false);

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  };

  const handleEmailChange = (event) => {
    const value = event.target.value;
    setEmail(value);
  };

  const handleConferenceChange = (event) => {
    const value = event.target.value;
    setSelectedConference(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.name = name;
    data.email = email;
    data.conference = selectedConference;

    console.log(data);

    const attendeeUrl = "http://localhost:8001/api/attendees/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(attendeeUrl, fetchConfig);
    if (response.ok) {
      setSuccess(true);
      setName("");
      setEmail("");
      setSelectedConference("");
    }
  };

  const fetchData = async () => {
    const url = "http://localhost:8000/api/conferences/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setConferences(data.conferences);
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  // Decide classes based on conferences data
  let spinnerClasses = "d-flex justify-content-center mb-3";
  let dropdownClasses = "form-select d-none";
  if (conferences.length > 0) {
    spinnerClasses = "d-flex justify-content-center mb-3 d-none";
    dropdownClasses = "form-select";
  }

  if (loading) {
    return <div>Loading...</div>;
  }

  return (
    <div className="my-5 container">
      <div className="col">
        <form onSubmit={handleSubmit} id="create-attendee-form">
          <h1>It's Conference Time!</h1>
          <p>Please choose which conference you'd like to attend.</p>
          <div className={spinnerClasses}>
            <div className="spinner-grow text-secondary" role="status">
              <span className="visually-hidden">Loading...</span>
            </div>
          </div>
          <div className={dropdownClasses}>
            <img
              src="/logo.svg"
              alt="Conference GO! Logo"
              width="300"
              className="bg-white rounded shadow d-block mx-auto mb-4"
            />
            <select
              name="conference"
              id="conference"
              className="form-select"
              required
              value={selectedConference}
              onChange={handleConferenceChange}
            >
              <option value="">Choose a conference</option>
              {conferences.map((conference) => (
                <option key={conference.href} value={conference.href}>
                  {conference.name}
                </option>
              ))}
            </select>
          </div>
          <p>Now, tell us about yourself.</p>
          <div className="row">
            <div className="col">
              <div className="form-floating mb-3">
                <input
                  required
                  placeholder="Your full name"
                  type="text"
                  id="name"
                  name="name"
                  className="form-control"
                  value={name}
                  onChange={handleNameChange}
                />
                <label htmlFor="name">Your full name</label>
              </div>
            </div>
            <div className="col">
              <div className="form-floating mb-3">
                <input
                  required
                  placeholder="Your email address"
                  type="email"
                  id="email"
                  name="email"
                  className="form-control"
                  value={email}
                  onChange={handleEmailChange}
                />
                <label htmlFor="email">Your email address</label>
              </div>
            </div>
          </div>
          <button className="btn btn-lg btn-primary">I'm going!</button>
          {success && (
            <div className="alert alert-success mt-3">
              Congratulations! You're all signed up!
            </div>
          )}
        </form>
      </div>
    </div>
  );
}

export default AttendeeForm;
